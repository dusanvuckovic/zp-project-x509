package x509.util;

import java.util.*;

/**
 *
 * @author Aca
 */
public class TextChange {

    private static TextChange singleton = new TextChange();

    private TextChange() {
    }

    public TextChange getInstance() {
        return singleton;
    }

    private List<UpdateText> listeners = new ArrayList<UpdateText>();

    public static void addListener(UpdateText toAdd) {
        singleton.listeners.add(toAdd);
    }

    public static void updateAll() {
        for (UpdateText d : singleton.listeners) {
            d.updateText();
        }
    }

    public static void updateAll(String text) {
        for (UpdateText d : singleton.listeners) {
            d.updateText(text);
        }
    }
}
