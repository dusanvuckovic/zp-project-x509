package x509.extensions;

/**
 *
 * @author Dusan
 */
public class BasicConstraintsHelper {
    
    private final boolean CA, isCritical;
    private final int length;
    public BasicConstraintsHelper(boolean CA, boolean isCritical, int length) {
        this.CA = CA; this.isCritical = isCritical; this.length = length;
    }
    
    public boolean constructLength() {
        return CA && isCritical;
    }                
    
    public int getLength() {
        return length;            
    }
    
    public boolean isCA() {
        return CA;
    }
    
    public boolean isCritical() {
        return isCritical;
    }
    
}
