package x509.extensions;

import org.bouncycastle.asn1.x509.KeyUsage;

/**
 *
 * @author Dusan
 */
public class KeyUsageHelper {

    private final boolean isCritical;
    private final boolean dataEncipherment, encipherOnly, digitalSignature, decipherOnly, crlSign, keyAgreement, keyEnchipherment, contentCommitment, keyCertSign;

    public KeyUsageHelper(boolean isCritical, boolean dataEncipherment, boolean encipherOnly, boolean digitalSignature, boolean decipherOnly, boolean crlSign, boolean keyAgreement,
            boolean keyEnchipherment, boolean contentCommitment, boolean keyCertSign) {

        this.isCritical = isCritical;

        this.encipherOnly = encipherOnly;
        this.crlSign = crlSign;
        this.keyCertSign = keyCertSign;
        this.keyAgreement = keyAgreement;
        this.dataEncipherment = dataEncipherment;
        this.keyEnchipherment = keyEnchipherment;
        this.contentCommitment = contentCommitment; //non-repudiation            
        this.digitalSignature = digitalSignature;
        this.decipherOnly = decipherOnly;
    }
    
    public KeyUsageHelper(boolean isCritical, boolean[] key) {
        this.isCritical = isCritical;
        
        this.encipherOnly = key[0];
        this.crlSign = key[1];
        this.keyCertSign = key[2];
        this.keyAgreement = key[3];
        this.dataEncipherment = key[4];
        this.keyEnchipherment = key[5];
        this.contentCommitment = key[6]; //non-repudiation            
        this.digitalSignature = key[7];
        this.decipherOnly = key[8];    
    }

    public boolean isCritical() {
        return isCritical;
    }

    public int returnConstructorValue() {
        int start = 0;

        if (encipherOnly)
            start |= KeyUsage.encipherOnly;        
        if (crlSign)
            start |= KeyUsage.cRLSign;        
        if (keyCertSign)
            start |= KeyUsage.keyCertSign;       
        if (keyAgreement)
            start |= KeyUsage.keyAgreement;        
        if (dataEncipherment)
            start |= KeyUsage.dataEncipherment;       
        if (keyEnchipherment)
            start |= KeyUsage.keyEncipherment;        
        if (contentCommitment)
            start |= KeyUsage.nonRepudiation;        
        if (digitalSignature)
            start |= KeyUsage.digitalSignature;       
        if (decipherOnly)
            start |= KeyUsage.decipherOnly;       

        return start;
    }
}
