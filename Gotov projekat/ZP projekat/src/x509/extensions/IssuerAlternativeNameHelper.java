package x509.extensions;

/**
 *
 * @author Dusan
 */
public class IssuerAlternativeNameHelper {

    private final boolean isCritical;
    private final String alternativeName;

    public IssuerAlternativeNameHelper(boolean isCritical, String alternativeName) {
        this.isCritical = isCritical;
        this.alternativeName = alternativeName;
    }

    public boolean isCritical() {
        return isCritical;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

}
