package x509.main;

import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import x509.extensions.KeyUsageHelper;

public class SignExportCertificate {

    private static final String GOOD_CERT = "Certificate valid";

    private static final String basicConstraintsOID = "2.5.29.19";
    private static final String issuerAlternativeNameOID = "2.5.29.18";
    private static final String keyUsageOID = "2.5.29.15";
    
    private static final X500Name issuerX500Name = new X500Name("CN=zp2016ProjekatARDV");

    public SignExportCertificate() {
        myKeyStore = X509.getX509().getKeyStore();
    }

    private final KeyStore myKeyStore;

    public String signCertificate(PKCS10CertificationRequest myCSR,
            String certificateName,
            X500Name issuerX500Name,
            RSAPublicKey publicKey,
            RSAPrivateKey privateKey,
            char[] sessionPassword
    ) throws KeyStoreException, UnrecoverableKeyException {
        try {

            X509Certificate myCert = (X509Certificate) myKeyStore.getCertificate(certificateName);
            X509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(
                    issuerX500Name,
                    myCert.getSerialNumber(),
                    myCert.getNotBefore(),
                    myCert.getNotAfter(),
                    myCSR.getSubject(),
                    myCert.getPublicKey()
            );

            Set<String> criticalOIDs = myCert.getCriticalExtensionOIDs();
            Set<String> nonCriticalOIDs = myCert.getNonCriticalExtensionOIDs();

            HashMap<String, Boolean> extensions = new HashMap<>();

            for (String s : criticalOIDs) {
                extensions.put(s, Boolean.TRUE);
            }

            for (String s : nonCriticalOIDs) {
                extensions.put(s, Boolean.FALSE);
            }

            // Add basic constraints extension.
            if (extensions.containsKey(basicConstraintsOID)) {
                boolean isCritical = extensions.get(basicConstraintsOID);
                int stepsAllowed = myCert.getBasicConstraints();
                switch (stepsAllowed) {
                    case -1:
                        //CA = false, const = irrelevant
                        certBuilder.addExtension(Extension.basicConstraints,
                                isCritical,
                                new BasicConstraints(false));
                        break;
                    case Integer.MAX_VALUE:
                        //CA = true, const = irrelevant
                        certBuilder.addExtension(Extension.basicConstraints,
                                isCritical,
                                new BasicConstraints(true));
                        break;
                    default:
                        //CA = true, const = stepsAllowed
                        certBuilder.addExtension(Extension.basicConstraints,
                                isCritical,
                                new BasicConstraints(stepsAllowed));
                        break;
                }
            }

            // Add issuer alternative name extension.
            if (extensions.containsKey(issuerAlternativeNameOID)) {
                boolean isCritical = extensions.get(issuerAlternativeNameOID);
                Collection<List<?>> myList = myCert.getIssuerAlternativeNames();
                for (List<?> list : myList) {
                    String altName = (String) list.get(1);
                    certBuilder.addExtension(Extension.issuerAlternativeName, isCritical, new GeneralNames(new GeneralName(GeneralName.rfc822Name, altName)));
                    break; //treba dodati različite tipove imena (kao Integer u get(0)
                }
            }

            // Add key usage extension.
            if (extensions.containsKey(keyUsageOID)) {
                boolean isCritical = extensions.get(keyUsageOID);
                boolean[] keyUsage = myCert.getKeyUsage();
                KeyUsageHelper helper = new KeyUsageHelper(isCritical, keyUsage);
                int myValue = helper.returnConstructorValue();
                certBuilder.addExtension(Extension.keyUsage, isCritical, new KeyUsage(myValue));
            }

            JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder("SHA1WithRSA");
            signerBuilder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
            ContentSigner signer = signerBuilder.build(privateKey);
            X509CertificateHolder certHolder = certBuilder.build(signer);

            X509Certificate signedCert = new JcaX509CertificateConverter().getCertificate(certHolder);

            if (signedCert != null) {
                signedCert.verify(publicKey);
                Certificate certChain[] = {signedCert};
                PrivateKey privateKeyHolder = (RSAPrivateKey) myKeyStore.getKey(certificateName, sessionPassword);
                myKeyStore.setKeyEntry(certificateName, privateKeyHolder, sessionPassword, certChain);
                return GOOD_CERT;
            }

        } catch (OperatorCreationException | CertificateException | NoSuchProviderException | IOException | NoSuchAlgorithmException | InvalidKeyException | SignatureException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Error";
    }

    public boolean exportSignedCertificate(String selectedCertificate, String fileName) {

        try (FileWriter tempFW = new FileWriter(fileName); JcaPEMWriter pemWriter = new JcaPEMWriter(tempFW)) {
            X509Certificate myCertificate = (X509Certificate) myKeyStore.getCertificate(selectedCertificate);
            pemWriter.writeObject(myCertificate);
            return true;
        } catch (IOException | KeyStoreException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }

    public PKCS10CertificationRequest generateCSR(String keyStoreName, char[] sessionPassword) {
        try {
            // Get the certificate, the subject, and the keys.
            X509Certificate myCert = (X509Certificate) myKeyStore.getCertificate(keyStoreName);
            X500Principal subject = myCert.getSubjectX500Principal();
            RSAPublicKey publicKey = (RSAPublicKey) myCert.getPublicKey();
            RSAPrivateKey privateKey = (RSAPrivateKey) myKeyStore.getKey(keyStoreName, sessionPassword);

            // Generate a sign request and sign.
            JcaPKCS10CertificationRequestBuilder myCSRBuilder = new JcaPKCS10CertificationRequestBuilder(subject, publicKey);
            PKCS10CertificationRequest myCSR = myCSRBuilder.build(new JcaContentSignerBuilder("SHA1withRSA").setProvider("BC").build(privateKey));

            if (myCSR.isSignatureValid(new JcaContentVerifierProviderBuilder().setProvider("BC").build(publicKey))) {
                return myCSR;
            } else {
                return null;
            }
        } catch (PKCSException | OperatorCreationException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public X500Name getIssuerX500Name() {
        return issuerX500Name;
    }

}
