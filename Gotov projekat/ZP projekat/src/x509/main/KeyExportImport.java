package x509.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author dusan
 */
public class KeyExportImport {

    private SecretKeyFactory myKeyFactory = null;

    public KeyExportImport() {
        try {
            myKeyFactory = SecretKeyFactory.getInstance("PBEWithSHA256And128BitAES-CBC-BC", "BC");
        } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
            Logger.getLogger(KeyExportImport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean exportKeys(String keyToExport, String locationToExport,
            char[] password, KeyStore myKeyStore, char[] sessionPassword,
            byte[] sessionSeed, Cipher myCipher) {
        try {

            // Get the keys to be exported.        
            RSAPrivateKey privateKey = (RSAPrivateKey) myKeyStore.getKey(keyToExport, sessionPassword);
            X509Certificate selfSignedCertificate = (X509Certificate) myKeyStore.getCertificate(keyToExport);

            // Make a private store for the two keys we're exporting.
            KeyStore exportStore = KeyStore.getInstance("PKCS12");
            exportStore.load(null, null);

            // Place these keys into the store.
            Certificate[] certChain = {selfSignedCertificate};
            exportStore.setKeyEntry(locationToExport, privateKey, sessionPassword, certChain);

            // Write the store to the disk.
            FileOutputStream fos = new FileOutputStream(locationToExport);
            exportStore.store(fos, password);
            fos.close();

            // Read this raw file from the disk.
            byte[] keyData = Files.readAllBytes(Paths.get(locationToExport));

            // Create the AES key from the password.              
            PBEKeySpec keySpec = new PBEKeySpec(password, sessionSeed, 1);
            SecretKey key = myKeyFactory.generateSecret(keySpec);

            // Protect the file with AES.
            myCipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encoded = myCipher.doFinal(keyData);

            // Delete the previous file and write the new file to the disk.
            Files.delete(Paths.get(locationToExport));
            fos = new FileOutputStream(locationToExport);
            fos.write(encoded);
            fos.close();

            return true;
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException | CertificateException | IllegalBlockSizeException | InvalidKeyException | InvalidKeySpecException | BadPaddingException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
            try {
                // If there's an error, delete a file, if it exists.
                Files.delete(Paths.get(locationToExport));
            } catch (IOException ex1) {
                Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return false;
        }
    }

    public boolean importKeys(String importAs, String locationToImportFrom,
            char[] password, KeyStore myKeyStore, char[] sessionPassword,
            byte[] sessionSeed, Cipher myCipher) {
        try {
            // Read the encrypted file from the disk.
            byte[] encrypted = Files.readAllBytes(Paths.get(locationToImportFrom));

            // Generate an AES key from the file and the provided password.              
            PBEKeySpec keySpec = new PBEKeySpec(password, sessionSeed, 1);
            SecretKey key = myKeyFactory.generateSecret(keySpec);

            // Decrypt the AES encryption.
            myCipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decrypted = myCipher.doFinal(encrypted);

            // Delete the previous file and write new file to the disk.
            Files.delete(Paths.get(locationToImportFrom));
            FileOutputStream fos = new FileOutputStream(locationToImportFrom);
            fos.write(decrypted);
            fos.close();

            // Make a temporary key store and load the file containing encrypted keys.
            KeyStore importStore = KeyStore.getInstance("PKCS12");
            FileInputStream is = new FileInputStream(locationToImportFrom);
            importStore.load(is, password);

            // Take the encrypted key and the certificate from the file.            
            RSAPrivateKey privateKey = (RSAPrivateKey) importStore.getKey(locationToImportFrom, sessionPassword);
            X509Certificate certificate = (X509Certificate) importStore.getCertificate(locationToImportFrom);

            // Save the keys to the key store.               
            Certificate certChain[] = {certificate};
            myKeyStore.setKeyEntry(importAs, privateKey, sessionPassword, certChain);

            return true;

        } catch (FileNotFoundException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException | NoSuchAlgorithmException | CertificateException | KeyStoreException | UnrecoverableKeyException | InvalidKeySpecException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
