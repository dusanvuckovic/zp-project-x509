package x509.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class SerialsCache {

    private final TreeSet<BigInteger> serials = new TreeSet<>();

    public SerialsCache() {

    }
    
    public SerialsCache(String location) {
        loadFromDisk(location);
    }

    public void add(BigInteger serial) {
        serials.add(serial);
    }

    /**
     * Saves all existing serials to the file specified. If this file already
     * exists, it is overwritten.
     */
    public void saveToDisk(String location) {
        try {
            if (!Files.exists(Paths.get(location), LinkOption.NOFOLLOW_LINKS)) {
                return; //the file does not exist
            }
            if (serials.isEmpty()) {
                Files.delete(Paths.get(location)); // we delete an empty file
                return;
            }
            FileOutputStream fos = new FileOutputStream(location);
            ObjectOutputStream savedSerials = new ObjectOutputStream(fos);
            savedSerials.writeInt(serials.size());
            for (BigInteger bi : serials) {
                savedSerials.writeObject(bi);
            }
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

     /**
     * Loads all existing serials to the file specified.
     */
    public void loadFromDisk(String location) {
        try {
            if (!Files.exists(Paths.get(location), LinkOption.NOFOLLOW_LINKS)) {
                Files.createFile(Paths.get(location));
            } else {
                FileInputStream fis = new FileInputStream(location);
                ObjectInputStream savedSerials = new ObjectInputStream(fis);

                int howMuch = savedSerials.readInt();
                for (int i = 0; i < howMuch; i++) {
                    serials.add((BigInteger) savedSerials.readObject());
                }
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addSerial(BigInteger serial) {
        serials.add(serial);
    }

    boolean contains(BigInteger serial) {
        return serials.contains(serial);
    }

}
