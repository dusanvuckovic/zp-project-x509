package x509.main;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *
 * @author Dusan
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    private MainFrame() {
        initComponents();

    }

    private X509 x509 = X509.getX509();

    public static MainFrame createMainFrame() {
        mf = new MainFrame();
        mf.setResizable(false);
        mf.errorArea.setEditable(false);
        return mf;
    }

    public static MainFrame mf = null;

    public static void printErrorsInErrorArea(List<String> errors) {
        if (mf == null) {
            return;
        }
        mf.errorArea.setText("");
        mf.errorArea.setForeground(Color.red);
        for (String error : errors) {
            mf.errorArea.setText(mf.errorArea.getText() + error + "\n");
        }
    }

    public static void printLinesInErrorArea(Color color, List<String> lines) {
        if (mf == null) {
            return;
        }
        mf.errorArea.setText("");
        mf.errorArea.setForeground(color);
        for (String line : lines) {
            mf.errorArea.setText(mf.errorArea.getText() + line + "\n");
        }
    }

    public static void printErrorInErrorArea(String error) {
        if (mf == null) {
            return;
        }
        mf.errorArea.setForeground(Color.red);
        mf.errorArea.setText(error);
    }

    public static void printMessageInErrorArea(Color c, String msg) {
        if (mf == null) {
            return;
        }
        mf.errorArea.setForeground(c);
        mf.errorArea.setText(msg);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        tabHolder = new javax.swing.JTabbedPane();
        generatePanel = new x509.panels.KeyGeneratePanel();
        jPanel1 = new javax.swing.JPanel();
        exportPanel = new x509.panels.KeyExportPanel();
        importPanel = new x509.panels.KeyImportPanel();
        jSeparator1 = new javax.swing.JSeparator();
        certificateInfoPanel2 = new x509.panels.CertificateInfoPanel();
        jPanel2 = new javax.swing.JPanel();
        certificateSignPanel1 = new x509.panels.CertificateSignPanel();
        certificateIOPanel1 = new x509.panels.CertificateIOPanel();
        jSeparator2 = new javax.swing.JSeparator();
        viewKeystoreKeysButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        errorArea = new javax.swing.JTextArea();
        jSeparator3 = new javax.swing.JSeparator();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("x509 certificate manager");
        setLocationByPlatform(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        tabHolder.setFocusCycleRoot(true);
        tabHolder.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tabHolder.addTab("GENERATE KEY", generatePanel);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(exportPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)
            .addComponent(importPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(exportPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(importPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(150, 150, 150))
        );

        tabHolder.addTab("EXPORT/IMPORT KEY", jPanel1);
        tabHolder.addTab("CERTIFICATE INFO", certificateInfoPanel2);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(certificateSignPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)
            .addComponent(certificateIOPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(certificateSignPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(certificateIOPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE))
        );

        tabHolder.addTab("SIGN/EXPORT CERTIFICATE", jPanel2);

        viewKeystoreKeysButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        viewKeystoreKeysButton.setText("VIEW KEYSTORE KEYS");
        viewKeystoreKeysButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewKeystoreKeysButtonMouseClicked(evt);
            }
        });

        errorArea.setColumns(20);
        errorArea.setFont(new java.awt.Font("Lucida Console", 0, 14)); // NOI18N
        errorArea.setRows(5);
        jScrollPane2.setViewportView(errorArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabHolder)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(viewKeystoreKeysButton)
                        .addGap(196, 196, 196)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabHolder)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(viewKeystoreKeysButton)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        x509.saveSerials();
    }//GEN-LAST:event_formWindowClosing

    private void viewKeystoreKeysButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewKeystoreKeysButtonMouseClicked
        Enumeration<String> existingKeys = x509.getExistingKeys();
        if (existingKeys == null)
            MainFrame.printErrorInErrorArea("Keystore error: probably not initialized!");
        else if (!existingKeys.hasMoreElements())
            MainFrame.printErrorInErrorArea("The keystore is currently empty!");
        else {
            ArrayList<String> msgs = new ArrayList<>();
            msgs.add("Current keys are: \n");
            while (existingKeys.hasMoreElements()) {
                msgs.add("\n" + existingKeys.nextElement());
            }
            MainFrame.printLinesInErrorArea(Color.BLACK, msgs);
        }
    }//GEN-LAST:event_viewKeystoreKeysButtonMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private x509.panels.CertificateIOPanel certificateIOPanel1;
    private x509.panels.CertificateInfoPanel certificateInfoPanel2;
    private x509.panels.CertificateSignPanel certificateSignPanel1;
    private javax.swing.JTextArea errorArea;
    private x509.panels.KeyExportPanel exportPanel;
    private x509.panels.KeyGeneratePanel generatePanel;
    private x509.panels.KeyImportPanel importPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTabbedPane tabHolder;
    private javax.swing.JButton viewKeystoreKeysButton;
    // End of variables declaration//GEN-END:variables
}
