package x509.main;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import x509.extensions.BasicConstraintsHelper;
import x509.extensions.IssuerAlternativeNameHelper;
import x509.extensions.KeyUsageHelper;

/**
 *
 * @author dusan
 */
public class KeyGenerator {

    private final X509 x509 = X509.getX509();

    private X509Certificate generateSelfSignedX509Certificate(
            RSAPublicKey publicKey, RSAPrivateKey privateKey,
            LocalDateTime dateFrom, LocalDateTime dateTo,
            BigInteger serialNo, X500Principal holderPrincipal,
            BasicConstraintsHelper bc,
            IssuerAlternativeNameHelper ian,
            KeyUsageHelper ku) {

        X509Certificate cert = null;

        try {
            if (!BigInteger.ZERO.equals(serialNo)) { //if it's not a test
                x509.addSerial(serialNo);
            }

            // Initialize Bouncy Castle.
            X509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(
                    x509.getIssuer(), serialNo,
                    Utility.getDateFromLocalDateTime(dateFrom),
                    Utility.getDateFromLocalDateTime(dateTo),
                    holderPrincipal, publicKey);

            // Add basic constraints extension.
            if (bc != null) {
                if (bc.constructLength()) {
                    builder.addExtension(Extension.basicConstraints, bc.isCritical(), new BasicConstraints(bc.getLength())); //CA = true, const = bcnumber
                } else {
                    builder.addExtension(Extension.basicConstraints, bc.isCritical(), new BasicConstraints(bc.isCA())); //CA = true/false, const = irrelevant                            
                }
            }

            // Add issuer alternative name extension.
            if (ian != null) {
                builder.addExtension(Extension.issuerAlternativeName, ian.isCritical(), new GeneralNames(new GeneralName(GeneralName.rfc822Name, ian.getAlternativeName())));
            }

            // Add key usage extension.
            if (ku != null) {
                builder.addExtension(Extension.keyUsage, ku.isCritical(), new KeyUsage(ku.returnConstructorValue()));
            }

            // Make a Bouncy Castle certificate and sign it with private key.
            JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder("SHA1WithRSA");
            signerBuilder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
            ContentSigner signer = signerBuilder.build(privateKey);
            X509CertificateHolder certHolder = builder.build(signer);

            // Transform the Bouncy Castle certificate into a Java one.
            cert = new JcaX509CertificateConverter().getCertificate(certHolder);

        } catch (CertificateException | OperatorCreationException | CertIOException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cert;
    }

    public void generateX509(int keySize,
            LocalDateTime dateFrom, LocalDateTime dateTo,
            BigInteger serialNo,
            String CN, String OU, String O, String L, String ST, String C,
            BasicConstraintsHelper bc,
            IssuerAlternativeNameHelper ian,
            KeyUsageHelper ku,
            String generateAs, String encryptionAlgorithm,
            KeyStore myKeyStore,
            char[] sessionPassword
    ) {

        try {
            // Get the key generator.
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(
                    encryptionAlgorithm, BouncyCastleProvider.PROVIDER_NAME);
            keyPairGenerator.initialize(keySize);

            // Generate a new pair of keys.
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

            // Generate holder name.
            String holderName = generateHolderName(CN, OU, O, L, ST, C);
            X500Principal holderPrincipal = new X500Principal(holderName);

            // Create a chain of certificates needed.        
            X509Certificate cert = generateSelfSignedX509Certificate(publicKey, privateKey, dateFrom,
                    dateTo, serialNo, holderPrincipal, bc, ian, ku);
            Certificate certChain[] = {cert};

            // Add to store.
            myKeyStore.setKeyEntry(generateAs, privateKey, sessionPassword, certChain);

        } catch (KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Creates an appropriate string from the holder names.
     *
     * @return
     */
    private static String generateHolderName(String CN, String OU, String O, String L, String ST, String C) {

        StringBuilder result = new StringBuilder();

        addToHolder(result, CN, "CN");
        addToHolder(result, OU, "OU");
        addToHolder(result, O, "O");
        addToHolder(result, L, "L");
        addToHolder(result, ST, "ST");
        addToHolder(result, C, "C");

        return result.toString();
    }

    private static void addToHolder(StringBuilder sb, String val, String type) {
        if (!val.equals("")) {
            if (sb.length() != 0) {
                sb.append(", ");
            }
            sb.append(type);
            sb.append("=");
            sb.append(val);
        }
    }

    // Testing!
    public void testGenerate(String encryptionAlgorithm,
            KeyStore myKeyStore,
            char[] sessionPassword) {
        generateX509(2048, LocalDateTime.now(), LocalDateTime.now().plusYears(1), BigInteger.ZERO, "Slackers", "RTI", "ETF",
                "Serbian", "Bulevar Kralja Aleksandra 83", "SR",
                new BasicConstraintsHelper(true, true, 3),
                new IssuerAlternativeNameHelper(false, "zarko@etf.rs"),
                new KeyUsageHelper(false, false, false, false, false, false, false, false, false, false),
                "testKey", encryptionAlgorithm, myKeyStore, sessionPassword);
    }

}
