package x509.main;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Dusan
 */
public final class X509 {

    private final X500Principal issuer
            = new X500Principal("CN=zp2016ProjekatARDV, L=Belgrade, O=ETF, OU=RTI, C=RS, STREET=Bulevar Kralja Aleksandra 83");

    private final String encryptionAlgorithm = "RSA";

    private static final BigInteger serialNo = new BigInteger("1000");
    private static final int keySize = 2048;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private Cipher myCipher = null;
    private KeyStore myKeyStore;

    private static final String signature = "sha1WithRSAEncryption"; //RFC 7427

    private final char[] sessionPassword;
    private final byte[] sessionSeed;

    private RSAPrivateKey privateKey;
    private RSAPublicKey publicKey;

    private static final String SERIALS_LOCATION = "serials.zp";
    private SerialsCache serials;

    private X509() {
        sessionPassword = "zpJ40milj4niPredmetV3lik0mBr0juStud4nata".toCharArray();
        sessionSeed = "zpJ40milj4niPredmetV3lik0mBr0juStud4nata".getBytes();
        try {
            myCipher = Cipher.getInstance("PBEWithSHA256And128BitAES-CBC-BC", "BC");
            serials = new SerialsCache(SERIALS_LOCATION); // Loads previous serials.

            myKeyStore = KeyStore.getInstance("PKCS12");
            myKeyStore.load(null, null);

            KeyPairGenerator kpg = KeyPairGenerator.getInstance(
                    encryptionAlgorithm,
                    BouncyCastleProvider.PROVIDER_NAME);
            kpg.initialize(keySize);

            // Generate a new pair of keys.
            KeyPair keyPair = kpg.generateKeyPair();
            privateKey = (RSAPrivateKey) keyPair.getPrivate();
            publicKey = (RSAPublicKey) keyPair.getPublic();

        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | IOException | CertificateException | KeyStoreException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static X509 x509 = null;

    public static X509 getX509() {
        if (x509 == null) {
            x509 = new X509();
        }
        return x509;
    }

    public char[] getSessionPassword() {
        return sessionPassword;
    }

    public Cipher getCipher() {
        return myCipher;
    }

    public byte[] getSessionSeed() {
        return sessionSeed;
    }

    public String getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public KeyStore getKeyStore() {
        return myKeyStore;
    }

    public X509Certificate getCertificateInfo(String certificateName) {
        try {
            X509Certificate certHolder = (X509Certificate) myKeyStore.getCertificate(certificateName);
            return certHolder;
        } catch (KeyStoreException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public RSAPublicKey getPublicKey() {
        return publicKey;
    }

    public RSAPrivateKey getPrivateKey() {
        return privateKey;
    }

    public X500Principal getIssuer() {
        return issuer;
    }

    public void addSerial(BigInteger serial) {
        serials.add(serial);
    }

    /**
     * Reads existing serials (if they exist) from the file serials.zp,
     * initializing the keystore with them.
     */
    private void loadSerials() {
        serials.loadFromDisk(SERIALS_LOCATION);
    }

    public void saveSerials() {
        serials.saveToDisk(SERIALS_LOCATION);
    }

    public boolean isSerialUnique(BigInteger serial) {
        return !serials.contains(serial);
    }

    /**
     * Returns the keys currently existing in the keystore as an enumeration.
     *
     * @return All existing keys.
     */
    public Enumeration<String> getExistingKeys() {
        try {
            return myKeyStore.aliases();
        } catch (KeyStoreException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Does the key with a specific alias exist in the key store?
     *
     * @param alias given key.
     * @return Whether the given key exists.
     */
    public boolean existsInKeystore(String alias) {
        try {
            boolean contained = myKeyStore.containsAlias(alias);
            return contained;
        } catch (KeyStoreException ex) {
            Logger.getLogger(X509.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
