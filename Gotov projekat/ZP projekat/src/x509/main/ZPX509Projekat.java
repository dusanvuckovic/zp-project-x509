package x509.main;

import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ColorUIResource;

/**
 *
 * @author Dusan
 */
public class ZPX509Projekat {

    static MainFrame mf = null;
    
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
	    if ("Nimbus".equals(info.getName())) {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
            UIManager.put("TextField.inactiveBackground", new ColorUIResource(Color.WHITE));
        }
        Class.forName("x509.main.X509");
        MainFrame mf = MainFrame.createMainFrame();                
        mf.setVisible(true);
    }
}
